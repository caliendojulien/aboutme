import java.util.Arrays;

public class Caliendojulien {
    private String nom;
    private String prenom;
    private int age;
    private String[] competences;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String[] getCompetences() {
        return competences;
    }

    public void setCompetences(String[] competences) {
        this.competences = competences;
    }

    public Caliendojulien() {
        super();
    }

    public Caliendojulien(String nom, String prenom, int age, String[] competences) {
        super();
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.competences = competences;
    }

    @Override
    public String toString() {
        return "Caliendojulien [nom=" + nom + ", prenom=" + prenom + ", age=" + age + ", competences="
                + Arrays.toString(competences) + "]";
    }
}
